package com.programming.khanhdd.cms.service.login.impl;

import com.programming.khanhdd.cms.entity.role.UserCms;
import com.programming.khanhdd.cms.service.auth.RoleCmsService;
import com.programming.khanhdd.cms.service.login.AuthKeycloakService;
import com.programming.khanhdd.cms.utils.NameUtil;
import com.programming.khanhdd.exception.BusinessException;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AuthKeycloakServiceImpl implements AuthKeycloakService {
    private final Keycloak keycloak;
    @Value("${keycloak.realm}")
    private String authRealm;

    @Autowired
    RoleCmsService roleCmsService;
    public AuthKeycloakServiceImpl(@Qualifier("keycloakCms") Keycloak keycloak) {
        this.keycloak = keycloak;
    }

    @Override
    public List<UserRepresentation> findUserKeycloakByUserName(String userName) {
        try {
            return keycloak.realm(authRealm).users().search(userName);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserRepresentation createUserOnKeycloak(UserCms userCmsCommand, String password, String roleName) {
        Response keycloakResponse = keycloak.realm(authRealm)
                .users().create(createUserRepresentation(userCmsCommand, convertCredential(password)));
        Map<String, UserRepresentation> mapUserKeycloak = null;
        if (keycloakResponse.getStatus() == HttpStatus.CREATED.value()) {
            List<UserRepresentation> userKeycloakByUserName = findUserKeycloakByUserName(userCmsCommand.getUserName());

            if (null != userKeycloakByUserName && !userKeycloakByUserName.isEmpty()) {
                mapUserKeycloak = userKeycloakByUserName.stream().collect(Collectors.toMap(UserRepresentation::getUsername, Function.identity()));
                if (null == mapUserKeycloak.get(userCmsCommand.getUserName())) {
                    throw new BusinessException("User không tồn tại trên keycloak");
                }
            }
            //gán quyền user trên Keyclaok
            roleCmsService.assignRole(mapUserKeycloak.get(userCmsCommand.getUserName()).getId(), roleName);
        } else {
            throw new BusinessException("Lỗi khi tạo user trên keycloak");
        }
        return mapUserKeycloak.get(userCmsCommand.getUserName());
    }

    @Override
    public void deleteUserOnKeycloak(String userId) {
        keycloak.realm(authRealm).users().delete(userId);
    }

    private UserRepresentation createUserRepresentation(UserCms userCmsCommand,
                                                        CredentialRepresentation credential) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(userCmsCommand.getUserName());
        userRepresentation.setCredentials(List.of(credential));
        userRepresentation.setFirstName(NameUtil.getFirstName(userCmsCommand.getFullName()));
        userRepresentation.setLastName(NameUtil.getLastName(userCmsCommand.getFullName()));
        userRepresentation.setEmail(userCmsCommand.getEmail());
        userRepresentation.setEnabled(true);
        userRepresentation.setEmailVerified(false);
        Map<String, List<String>> userAttributes = new HashMap<>();
        userAttributes.put("cif", List.of(userCmsCommand.getCifNumber()));
        userAttributes.put("branch", List.of(userCmsCommand.getBranchId()));
        userRepresentation.setAttributes(userAttributes);
        return userRepresentation;
    }

    private CredentialRepresentation convertCredential(String password) {
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setTemporary(false);
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(password);
        return credential;
    }
}
