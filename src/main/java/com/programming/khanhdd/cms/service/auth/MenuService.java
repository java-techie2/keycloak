package com.programming.khanhdd.cms.service.auth;

import com.programming.khanhdd.cms.entity.role.MenuCms;
import com.programming.khanhdd.cms.model.request.menu.CreateMenuCmsRequest;
import com.programming.khanhdd.cms.model.response.menu.CreateMenuCmsResponse;

import java.time.LocalDateTime;

public interface MenuService {
    CreateMenuCmsResponse createMenu(CreateMenuCmsRequest request);

    default MenuCms buildDataMenu(CreateMenuCmsRequest request, String userName) {
        MenuCms data = MenuCms.builder()
                .menuCode(request.getMenuCode().strip())
                .menuName(request.getMenuName().strip())
                .menuUrl(request.getMenuUrl().strip())
                .iconCode(request.getIcon())
                .description(request.getDescription())
                .parentCode(request.getParentCode())
                .rank(request.getRank())
                .build();
        data.setCreatedDate(LocalDateTime.now());
        data.init(userName);
        return data;
    }
}
