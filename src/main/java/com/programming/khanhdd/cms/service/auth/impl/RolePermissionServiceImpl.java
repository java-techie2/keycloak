package com.programming.khanhdd.cms.service.auth.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.programming.khanhdd.cms.cache.AuthorizationCache;
import com.programming.khanhdd.cms.dto.AuthorizationInformation;
import com.programming.khanhdd.cms.entity.role.MenuCms;
import com.programming.khanhdd.cms.entity.role.Permission;
import com.programming.khanhdd.cms.entity.role.RoleCms;
import com.programming.khanhdd.cms.entity.role.RolePermission;
import com.programming.khanhdd.cms.repository.MenuRepository;
import com.programming.khanhdd.cms.repository.PermissionRepository;
import com.programming.khanhdd.cms.repository.RoleCmsRepository;
import com.programming.khanhdd.cms.repository.RolePermissionRepository;
import com.programming.khanhdd.cms.service.auth.RolePermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@Slf4j
@Service
public class RolePermissionServiceImpl implements RolePermissionService {
    @Autowired
    RolePermissionRepository rolePermissionRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    AuthorizationCache authorizationCache;

    @Autowired
    RoleCmsRepository roleCmsRepository;
    public List<RolePermission> saveAll(List<RolePermission> rolePermission) {
        return rolePermissionRepository.saveAll(rolePermission);
    }

    @Override
    public HashMap<String, ArrayList<String>> getPermissionOnCache(String roleCode) {
        HashMap<String, ArrayList<String>> permission= new HashMap<>();
        RoleCms roleCms = roleCmsRepository.getCmsRoleByCode(roleCode);
        if(!Objects.isNull(roleCms)){
            List<RolePermission> rolePermission = rolePermissionRepository.getRolePermissionByRoleId(roleCms.getId());
            ArrayList<Permission> listPer = new ArrayList<>();
            rolePermission.stream().forEach(
                    i -> {
                        Permission permission1 = permissionRepository.findPermissionById(i.getPermissionId());
                        listPer.add(permission1);
                        MenuCms menuCms = menuRepository.getById(listPer.stream().map(Permission::getMenuId).distinct().findFirst().orElse(null));
                        permission.put(menuCms.getMenuCode(), new ArrayList<>(listPer.stream().map(Permission::getAction).collect(Collectors.toList())));
                    }
            );
        }

        return permission;
    }

    @Override
    public HashMap<String, ArrayList<String>> getPermissionOnCacheV1(String roleCode) {
        HashMap<String, ArrayList<String>> permission = authorizationCache.getInfoRolePermissionOnCache(roleCode);

        if (null != permission) {
            return permission;
        } else {
            List<AuthorizationInformation> authorizationInformations = permissionRepository.getPermissionActionByRoleName(roleCode);
            try {
                String data = authorizationCache.buildData(authorizationInformations);
                authorizationCache.pushInfoRolePermissionOnCache(roleCode, new ObjectMapper().writeValueAsString(data));
            } catch (JsonProcessingException error) {
                log.error("", error);
            }
        }

        return permission;
    }
}
