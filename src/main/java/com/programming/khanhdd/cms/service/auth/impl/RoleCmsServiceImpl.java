package com.programming.khanhdd.cms.service.auth.impl;

import com.programming.khanhdd.cms.entity.role.RoleCms;
import com.programming.khanhdd.cms.repository.RoleCmsRepository;
import com.programming.khanhdd.cms.model.request.permission.CreatePermissionRequest;
import com.programming.khanhdd.cms.model.request.role.CreateRoleCmsRequest;
import com.programming.khanhdd.cms.model.response.role.CreateRoleCmsResponse;
import com.programming.khanhdd.cms.service.auth.RoleCmsService;
import com.programming.khanhdd.security.jwt.JwtTokenProvider;
import com.programming.khanhdd.security.jwt.JwtUtil;
import com.programming.khanhdd.util.ModelMapperUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
public class RoleCmsServiceImpl implements RoleCmsService {
    private final Keycloak keycloak;
    @Value("${keycloak.realm}")
    private String authRealm;
    @Autowired
    RoleCmsRepository roleCmsRepository;
    @Autowired
    JwtTokenProvider tokenProvider;

    public RoleCmsServiceImpl(@Qualifier("keycloakCms") Keycloak keycloak) {
        this.keycloak = keycloak;
    }

    public RoleCms saveRoleCms(CreatePermissionRequest request) {
        RoleCms roleCms = RoleCms.builder()
                .description(request.getDescription())
                .roleName(request.getRoleName().trim())
                .roleCode(request.getRoleName().trim().replace(" ", "_"))
                .build();
        roleCms.init(tokenProvider.getUsernameFromToken(JwtUtil.getBearerToken()));
        this.createRole(request.getRoleName(), request.getDescription());
        return roleCmsRepository.save(roleCms);
    }

    @Override
    public RoleRepresentation findRoleByName(String roleName) {
        return keycloak.realm(authRealm).roles().get(roleName).toRepresentation();
    }

    @Override
    public RoleCms findByRoleName(String roleName) {
        return roleCmsRepository.findRoleCmsByRoleName(roleName);
    }

    @Override
    public RoleCms getRoleCmsByCode(String code) {
        return roleCmsRepository.getCmsRoleByCode(code);
    }

    @Override
    public boolean createRole(String roleName, String description) {
        RoleRepresentation role = new RoleRepresentation();
        role.setName(roleName);
        role.setDescription(description);
        keycloak.realm(authRealm).roles().create(role);
        return true;
    }

    @Override
    public boolean assignRole(String userId, String roleName) {
        RoleRepresentation role = findRoleByName(roleName);
        try {
            keycloak.realm(authRealm).users()
                    .get(userId).roles().realmLevel().add(Collections.singletonList(role));
        } catch (Exception e) {
            throw e;
        }
        return false;
    }

    @Override
    public boolean deleteRoleUser(String userId, String roleName) {
        RoleRepresentation role = findRoleByName(roleName);
        try {
            keycloak.realm(authRealm).users()
                    .get(userId).roles().realmLevel().remove(Collections.singletonList(role));
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    @Override
    public RoleCms saveRoleCms(RoleCms roleCms) {
        return roleCmsRepository.save(roleCms);
    }

    @Override
    @Transactional
    public CreateRoleCmsResponse createRoleCms(CreateRoleCmsRequest request) {
        RoleCms roleCms = ModelMapperUtil.mapper(request, RoleCms.class);
        roleCms.setCreatedBy(tokenProvider.getUsernameFromToken(JwtUtil.getBearerToken()));
        roleCms.init(tokenProvider.getUsernameFromToken(JwtUtil.getBearerToken()));
        roleCms = roleCmsRepository.save(roleCms);

        this.createRole(request.getRoleName(), request.getDescription());
        return ModelMapperUtil.mapper(roleCms, CreateRoleCmsResponse.class);
    }
}
