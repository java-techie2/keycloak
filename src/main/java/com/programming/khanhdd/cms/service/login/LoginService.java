package com.programming.khanhdd.cms.service.login;

import com.programming.khanhdd.cms.model.request.login.*;
import com.programming.khanhdd.cms.model.response.login.*;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.http.ResponseEntity;

public interface LoginService {
    ResponseEntity<LoginResponse> login(LoginRequest loginrequest);
    ResponseEntity<Response> logout(TokenRequest request);
    ResponseEntity<IntrospectResponse> introspect(TokenRequest request);
    AccessTokenResponse loginV1(LoginRequest request);
}
