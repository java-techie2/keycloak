package com.programming.khanhdd.cms.service.auth.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.programming.khanhdd.cms.cache.AuthorizationCache;
import com.programming.khanhdd.cms.dto.AuthorizationInformation;
import com.programming.khanhdd.cms.dto.PermissionAction;
import com.programming.khanhdd.cms.dto.PermissionOnRole;
import com.programming.khanhdd.cms.entity.role.Permission;
import com.programming.khanhdd.cms.entity.role.RoleCms;
import com.programming.khanhdd.cms.entity.role.RolePermission;
import com.programming.khanhdd.cms.repository.PermissionRepository;
import com.programming.khanhdd.cms.model.request.permission.CreatePermissionRequest;
import com.programming.khanhdd.cms.model.request.role.PushPermissionRedisRequest;
import com.programming.khanhdd.cms.model.response.permission.CreatePermissionResponse;
import com.programming.khanhdd.cms.model.response.role.PushPermissionRedisResponse;
import com.programming.khanhdd.cms.service.auth.PermissionActionService;
import com.programming.khanhdd.cms.service.auth.RoleCmsService;
import com.programming.khanhdd.cms.service.auth.RolePermissionService;
import com.programming.khanhdd.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class PermissionActionServiceImpl implements PermissionActionService {
    @Autowired
    RoleCmsService roleCmsService;
    @Autowired
    PermissionRepository permissionRepository;
    @Autowired
    RolePermissionService rolePermissionService;
    @Autowired
    AuthorizationCache authorizationCache;

    public CreatePermissionResponse addRolePermission(CreatePermissionRequest request) {
        RoleCms roleCms = null;
        roleCms = roleCmsService.findByRoleName(request.getRoleName());
        if (Objects.isNull(roleCms)) {
            roleCms = roleCmsService.saveRoleCms(request);
        }
        List<RolePermission> rolePermissions = new ArrayList<>();
        for (PermissionOnRole element : request.getListPermissionOnRole()) {
            for (PermissionAction e : element.getListAction()) {
                Permission permission = permissionRepository.findByMenuIdAndAction(element.getIdMenu(), e.getAction());
                log.debug(" --permission {}:", permission);
                if (!Objects.isNull(permission)) {
                    RolePermission rolePermission = new RolePermission();
                    rolePermission.setPermissionId(permission.getId());
                    rolePermission.setRoleId(roleCms.getId());
                    rolePermission.setRoleId(roleCms.getId());
                    rolePermission.setId(e.getId());
                    rolePermission.setDeleted(e.isDeleted());
                    rolePermissions.add(rolePermission);
                }
            }
        }
        rolePermissionService.saveAll(rolePermissions);
        return new CreatePermissionResponse("Thêm thành công");
    }

    @Override
    public PushPermissionRedisResponse addRoleRedis(PushPermissionRedisRequest request) {
        List<AuthorizationInformation> authorizationInformationList = this.getPermissionActionByRoleName(request.getRoleName());
        try {
            String data = authorizationCache.buildData(authorizationInformationList);
            // đẩy data lên redis
            authorizationCache.pushInfoRolePermissionOnCache(request.getRoleName().trim().replace(StringUtil.SPACE, StringUtil.UNDERSCORE), data);
        } catch (JsonProcessingException error) {
            log.error("", error);
        }
        return null;
    }

    @Override
    public List<AuthorizationInformation> getPermissionActionByRoleName(String roleName) {
        return permissionRepository.getPermissionActionByRoleName(roleName);
    }
}
