package com.programming.khanhdd.cms.service;

import com.programming.khanhdd.cms.model.user.CreateUserRequest;
import com.programming.khanhdd.cms.entity.Users;
import com.programming.khanhdd.cms.repository.UsersRepository;
import com.programming.khanhdd.cms.service.login.AuthKeycloakService;

import com.programming.khanhdd.security.jwt.JwtTokenProvider;

import com.programming.khanhdd.security.jwt.JwtUtil;
import com.programming.khanhdd.util.PasswordUtil;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    UsersRepository usersRepository;

    @Autowired
    AuthKeycloakService authKeycloakService;

    @Autowired
    JwtTokenProvider tokenProvider;


    public UserService(JwtTokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    public List<Users> getAllUsers (){
        return usersRepository.findAll();
    }
    public Users getUserDetail(String id){
        List<UserRepresentation> listUser = authKeycloakService.findUserKeycloakByUserName("max");
        return usersRepository.findById(id).get();
    }

    public Users saveUser(CreateUserRequest request) throws Exception {
       Users user = Users.builder()
               .id(UUID.randomUUID().toString())
               .userName(request.getUserName())
               .fullName(request.setFullName())
               .password(PasswordUtil.encryptPassword(request.getPassword()))
               .enabled(true)
               .email(request.getEmail())
               .build();
        List<UserRepresentation> listUser = authKeycloakService.findUserKeycloakByUserName(request.getUserName());
        String fullName = tokenProvider.getUsernameFromToken(JwtUtil.getBearerToken());
        return usersRepository.save(user);
    }

}
