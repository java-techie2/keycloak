package com.programming.khanhdd.cms.service;

import com.programming.khanhdd.cms.entity.Employee;
import com.programming.khanhdd.cms.repository.EmployeeRepository;
import com.programming.khanhdd.cms.model.request.user.EmployeeRequest;
import com.programming.khanhdd.exception.BusinessException;
import com.programming.khanhdd.util.CheckStringUtil;
import com.programming.khanhdd.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.*;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    public List<Employee> getAll() {
        List<Employee> employees = employeeRepository.getAllByIsDelete(false);
        return employees;
    }

    public Employee getDetail(String id) {
        Employee employee = employeeRepository.getEmployeeByIdAndIsDelete(id, false);
        if (Objects.isNull(employee)) {
            throw new BusinessException("NOT_FOUND","Không tìm thấy nhân viên");
        }
        return employee;
    }

    public Employee createEmployee(EmployeeRequest request) {
        Employee employee = Employee.builder()
                .id(UUID.randomUUID().toString())
                .fullName(request.getFullName())
                .userName(request.getUserName())
                .email(request.getEmail())
                .isDelete(false)
                .build();
        return employeeRepository.save(employee);
    }

    public Employee updateEmployee(EmployeeRequest request, String id) {
        Employee employee = this.getDetail(id);
        employee.setEmail(request.getEmail());
        employee.setFullName(request.getFullName());
        employee.setUserName(request.getUserName());
        return employeeRepository.save(employee);
    }

    public Employee updateEmployeeByFields(String id, EmployeeRequest request) throws IllegalAccessException {
        Optional<Employee> existingEmployee = employeeRepository.findById(id);
        Map<String, Object> fields = ModelMapperUtil.convert(request);
        if (existingEmployee.isPresent()) {
            fields.forEach((key, value) -> {
                Field field = ReflectionUtils.findField(Employee.class, key);
                field.setAccessible(true);
                ReflectionUtils.setField(field, existingEmployee.get(), value);
            });
            return employeeRepository.save(existingEmployee.get());
        }
        return null;
    }


    public Employee deleteEmployee(String id) {
        Employee employee = this.getDetail(id);
        employee.setIsDelete(true);
        return employeeRepository.save(employee);
    }

    public boolean checkExist(String userName, String email){
        if (CheckStringUtil.checkNull(userName) == null && CheckStringUtil.checkNull(email) == null){
            throw new BusinessException("ERROR_INPUT","Cần nhập userName và email");
        }
        else if (CheckStringUtil.checkNull(userName) == null && CheckStringUtil.checkNull(email) != null){
            if (employeeRepository.existsByEmailAndIsDelete(email, false)){
                throw new BusinessException("ERROR_EMAIL", "Email đã tồn tại");
            }
        }
        else if (CheckStringUtil.checkNull(email) == null && CheckStringUtil.checkNull(userName) != null){
            if (employeeRepository.existsByUserNameAndIsDelete(userName, false)){
                throw new BusinessException("ERROR_USERNAME","User name đã tồn tại");
            }
        }
        else if (CheckStringUtil.checkNull(email) != null && CheckStringUtil.checkNull(userName) != null){
            if (employeeRepository.existsByUserNameAndIsDelete(userName, false)){
                throw new BusinessException("ERROR_USERNAME","User name đã tồn tại");
            }
            if (employeeRepository.existsByEmailAndIsDelete(email, false)){
                throw new BusinessException("ERROR_EMAIL", "Email đã tồn tại");
            }
        }
        return false;
    }
}
