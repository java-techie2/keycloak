package com.programming.khanhdd.cms.service.login;

import com.programming.khanhdd.cms.entity.role.UserCms;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;

public interface AuthKeycloakService {
    List<UserRepresentation> findUserKeycloakByUserName(String userName);

    UserRepresentation createUserOnKeycloak(UserCms userCmsCommand, String password, String roleName);

    void deleteUserOnKeycloak(String userId);
}
