package com.programming.khanhdd.cms.service.auth.impl;

import com.programming.khanhdd.cms.dto.UserCmsDTO;
import com.programming.khanhdd.cms.dto.UserRoleProcedureEntity;
import com.programming.khanhdd.cms.dto.UserRoleViewEntity;
import com.programming.khanhdd.cms.entity.role.RoleCms;
import com.programming.khanhdd.cms.entity.role.RoleUser;
import com.programming.khanhdd.cms.entity.role.UserCms;
import com.programming.khanhdd.cms.model.request.user.CreateUserCmsRequest;
import com.programming.khanhdd.cms.repository.RoleUserRepository;
import com.programming.khanhdd.cms.repository.UserCmsRepository;
import com.programming.khanhdd.cms.repository.UserRoleRepository;
import com.programming.khanhdd.cms.repository.UserRoleRepositoryV1;
import com.programming.khanhdd.cms.service.auth.RoleCmsService;
import com.programming.khanhdd.cms.service.auth.RolePermissionService;
import com.programming.khanhdd.cms.service.auth.UserCmsService;
import com.programming.khanhdd.cms.service.login.AuthKeycloakService;
import com.programming.khanhdd.cms.service.noti.NotificationService;
import com.programming.khanhdd.cms.utils.PasswordUtils;
import com.programming.khanhdd.cms.utils.ValidateUtils;
import com.programming.khanhdd.exception.BusinessException;
import com.programming.khanhdd.security.jwt.JwtTokenProvider;
import com.programming.khanhdd.security.jwt.JwtUtil;
import io.netty.util.internal.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserCmsServiceImpl implements UserCmsService {

    private final JwtTokenProvider tokenProvider;
    @Autowired
    UserCmsRepository userCmsRepository;
    @Autowired
    UserRoleRepositoryV1 userRoleRepositoryV1;
    @Autowired
    UserRoleRepository userRoleRepository;
    @Autowired
    AuthKeycloakService authKeycloakService;
    @Autowired
    RoleCmsService roleCmsService;
    @Autowired
    RolePermissionService rolePermissionService;
    @Autowired
    RoleUserRepository roleUserRepository;

    @Autowired
    NotificationService notificationService;

    @Override
    public UserCms saveUser(CreateUserCmsRequest request) {
        if (ValidateUtils.validateEmail(request.getEmail().strip()) == false) {
            throw new BusinessException("Email không đúng định dạng");
        }
        List<UserCmsDTO> userCms = this.findUserByUserNameOrEmailOrCifNumber
                (request.getUserName().strip(), request.getEmail().strip(), request.getCifNumber().strip());
        if (null != userCms || !userCms.isEmpty()) {
            userCms.stream().forEach(
                    i -> {
                        if (request.getEmail().equals(i.getEmail())) {
                            throw new BusinessException("Email đã tồn tại");
                        } else if (request.getUserName().equals(i.getUserName())) {
                            throw new BusinessException("UserName đã tồn tại");
                        } else if (request.getCifNumber().equals(i.getCifNumber())) {
                            throw new BusinessException("Số cif đã tồn tại");
                        }
                    }
            );
        }
        List<UserRepresentation> userKeycloakByUserName = authKeycloakService.findUserKeycloakByUserName(request.getUserName());
        if (!userKeycloakByUserName.isEmpty()) {
            for (UserRepresentation userRepresentation : userKeycloakByUserName) {
                if (null != userRepresentation && userRepresentation.getUsername().equals(request.getUserName())) {
                    throw new BusinessException("User đã tồn tại trên keycloak");
                }
            }
        }

        String passWord = PasswordUtils.createPassword(6);
        String hashPassword = null;
        try {
            hashPassword = PasswordUtils.encryptPassword(passWord);
        } catch (Exception e) {
            log.info("Co loi khi tao mat khau");
        }
        UserCms userCmsCommand = UserCms.builder()
                .branchId(request.getBranchId())
                .email(request.getEmail())
                .cifNumber(request.getCifNumber())
                .fullName(request.getFullName())
                .userName(request.getUserName())
                .password(hashPassword)
                .build();
        userCmsCommand.init(tokenProvider.getUsernameFromToken(JwtUtil.getBearerToken()));
        // thêm mới user trên cả Keyclaok
        //find role name by rolecode
        RoleCms roleCms = roleCmsService.getRoleCmsByCode(request.getRoleCode());
        if (null == roleCms) {
            throw new BusinessException("Không tìm thấy role");
        }
        UserRepresentation userOnKeycloak = authKeycloakService.createUserOnKeycloak(userCmsCommand, hashPassword, roleCms.getRoleName());
        userCmsCommand.setUserId(userOnKeycloak.getId());
        try {
            userCmsCommand = userCmsRepository.save(userCmsCommand);
        } catch (Exception e) {
            this.rollBackAndDeleteUser(userOnKeycloak.getId());
            log.info("Có lỗi khi tạo user");
            throw e;
        }

        //insert vào bảng role_user
        RoleUser ru = new RoleUser();
        ru.setUserId(userCmsCommand.getId());
        ru.setRoleId(request.getRoleId());
        ru.init(tokenProvider.getUsernameFromToken(JwtUtil.getBearerToken()));
        ru.setDescription(null == request.getDescription() ? StringUtil.EMPTY_STRING : request.getDescription());

        // thêm mới quyền trên cms
        roleUserRepository.save(ru);
//        CreateEmailNotificationRequest sendEmail = CreateEmailNotificationRequest.builder()
//                .email(userCmsCommand.getEmail())
//                .password(hashPassword)
//                .userName(userCmsCommand.getUserName())
//                .build();
//        notificationService.sendMailViaKafka(sendEmail);
        return userCmsCommand;
    }


    @Override
    public List<UserCmsDTO> findUserByUserNameOrEmailOrCifNumber(String userName, String email, String cifNumber) {
        return userCmsRepository.findByUserNameOrEmailOrCifNumber(userName, email, cifNumber);
    }

    @Override
    public List<UserCms> findAll() {
        List<UserRoleViewEntity> viewEntityList = userRoleRepository.getAllByUserName("khanhdd03");
        List<UserRoleProcedureEntity> procedureEntities = userRoleRepositoryV1.getAllByUserNameProcedure("khanhdd03");
        return userCmsRepository.findAll();
    }

    private void rollBackAndDeleteUser(String userId) {
        authKeycloakService.deleteUserOnKeycloak(userId);
    }
}
