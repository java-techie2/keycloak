package com.programming.khanhdd.cms.service.noti.impl;

import com.programming.khanhdd.cms.dto.CreateEmailNotificationRequest;
import com.programming.khanhdd.cms.service.kafka.KafkaProducer;
import com.programming.khanhdd.cms.service.noti.NotificationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    KafkaProducer kafkaProducer;

    @Override
    public void sendMailViaKafka(CreateEmailNotificationRequest data) {
        kafkaProducer.produceMessageToNotify(data);
    }
}
