package com.programming.khanhdd.cms.service.auth.impl;

import com.programming.khanhdd.cms.entity.role.ActionCms;
import com.programming.khanhdd.cms.repository.ActionRepository;
import com.programming.khanhdd.cms.model.response.action.ActionResponse;

import com.programming.khanhdd.cms.service.auth.ActionService;
import com.programming.khanhdd.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActionServiceImpl implements ActionService {
    @Autowired
    ActionRepository actionRepository;

    public List<ActionResponse> getAllAction (){
        List<ActionCms> actionCmsList = actionRepository.findAll();
        List<ActionResponse> responseList = ModelMapperUtil.mapList(actionCmsList, ActionResponse.class);
        responseList.stream().forEach(i -> i.setActive(true));
        return responseList;
    }

    public ActionResponse getDetailAction(String actionCode){
        ActionCms actionCms = actionRepository.getActionCmsByActionCode(actionCode);
        return ModelMapperUtil.mapper(actionCms, ActionResponse.class);
    }
}
