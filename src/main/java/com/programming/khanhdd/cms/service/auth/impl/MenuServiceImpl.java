package com.programming.khanhdd.cms.service.auth.impl;

import com.programming.khanhdd.cms.dto.Action;
import com.programming.khanhdd.cms.entity.role.MenuAction;
import com.programming.khanhdd.cms.entity.role.MenuCms;
import com.programming.khanhdd.cms.entity.role.Permission;
import com.programming.khanhdd.cms.repository.ActionRepository;
import com.programming.khanhdd.cms.repository.MenuActionRepository;
import com.programming.khanhdd.cms.repository.MenuRepository;
import com.programming.khanhdd.cms.repository.PermissionRepository;
import com.programming.khanhdd.cms.model.request.menu.CreateMenuCmsRequest;
import com.programming.khanhdd.cms.model.response.menu.CreateMenuCmsResponse;
import com.programming.khanhdd.cms.service.auth.MenuService;
import com.programming.khanhdd.security.jwt.JwtTokenProvider;
import com.programming.khanhdd.security.jwt.JwtUtil;
import com.programming.khanhdd.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    MenuRepository menuRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    ActionRepository actionRepository;

    @Autowired
    MenuActionRepository menuActionRepository;

    private final JwtTokenProvider tokenProvider;

    public MenuServiceImpl(JwtTokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    public CreateMenuCmsResponse createMenu (CreateMenuCmsRequest request){

        // save menu cms va luu them vao bang menu_action
        String userName = tokenProvider.getUsernameFromToken(JwtUtil.getBearerToken());
        MenuCms menuCmsCommand = menuRepository.save(buildDataMenu(request, userName));
        List<MenuAction> listActionMenu = new ArrayList<>();
        List<Permission> listPermission = new ArrayList<>();
        for (Action element : request.getAction()) {
            //insert data vao bang menu_action
            MenuAction menuActionCommand = new MenuAction();
            menuActionCommand.init(userName);
            menuActionCommand.setMenuId(menuCmsCommand.getId());
            menuActionCommand.setActionId(element.getId());
            menuActionCommand.setActive(element.getActive()); //1 la active, 0 la inactive
            listActionMenu.add(menuActionCommand);
            // bổ sung mặc định insert các bản ghi vào bảng permission;
            Permission permission = new Permission();
            permission.setMenuId(menuCmsCommand.getId());
            permission.setAction(element.getActionCode());
            permission.setDeleted(false);
            permission.setActionName(element.getActionName());
            permission.init(userName);
            listPermission.add(permission);
        }
        permissionRepository.saveAll(listPermission);
        menuActionRepository.saveAll(listActionMenu);

        return ModelMapperUtil.mapper(menuCmsCommand, CreateMenuCmsResponse.class);
    }
}
