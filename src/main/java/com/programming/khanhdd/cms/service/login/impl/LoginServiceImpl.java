package com.programming.khanhdd.cms.service.login.impl;

import com.programming.khanhdd.cms.entity.role.UserCms;
import com.programming.khanhdd.cms.model.request.login.LoginRequest;
import com.programming.khanhdd.cms.model.request.login.TokenRequest;
import com.programming.khanhdd.cms.model.response.login.IntrospectResponse;
import com.programming.khanhdd.cms.model.response.login.LoginResponse;
import com.programming.khanhdd.cms.model.response.login.Response;
import com.programming.khanhdd.cms.repository.UserCmsRepository;
import com.programming.khanhdd.cms.service.login.LoginService;
import com.programming.khanhdd.exception.BusinessException;
import com.programming.khanhdd.security.jwt.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {
    private final Keycloak keycloak;
    private final JwtTokenProvider tokenProvider;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    UserCmsRepository userInfoRepository;
    @Value("${spring.security.oauth2.client.provider.keycloak.issuer-uri}")
    private String issueUrl;
    @Value("${spring.security.oauth2.client.registration.oauth2-client-credentials.client-id}")
    private String clientId;
    @Value("${spring.security.oauth2.client.registration.oauth2-client-credentials.client-secret}")
    private String clientSecret;
    @Value("${spring.security.oauth2.client.registration.oauth2-client-credentials.authorization-grant-type}")
    private String grantType;
    @Value("${keycloak.realm}")
    private String authRealm;
    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;
    @Value("${khanhdd.security.keycloak-realms.auth-realm.credentials.secret}")
    private String secret;

    public LoginServiceImpl(JwtTokenProvider tokenProvider, @Qualifier("keycloakCms") Keycloak keycloak) {
        this.tokenProvider = tokenProvider;
        this.keycloak = keycloak;
    }

    public ResponseEntity<LoginResponse> login(LoginRequest loginrequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", clientId);
        map.add("client_secret", clientSecret);
        map.add("grant_type", grantType);
        map.add("username", loginrequest.getUsername());
        map.add("password", loginrequest.getPassword());

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, headers);

        ResponseEntity<LoginResponse> response = restTemplate.postForEntity("http://localhost:8180/auth/realms/auth-realm/protocol/openid-connect/token", httpEntity, LoginResponse.class);
        return new ResponseEntity<>(response.getBody(), HttpStatus.OK);
    }

    public ResponseEntity<Response> logout(TokenRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", clientId);
        map.add("client_secret", clientSecret);
        map.add("refresh_token", request.getToken());


        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, headers);

        ResponseEntity<Response> response = restTemplate.postForEntity("http://localhost:8180/auth/realms/auth-realm/protocol/openid-connect/logout", httpEntity, Response.class);

        Response res = new Response();
        if (response.getStatusCode().is2xxSuccessful()) {
            res.setMessage("Logged out successfully");
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    public ResponseEntity<IntrospectResponse> introspect(TokenRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", clientId);
        map.add("client_secret", clientSecret);
        map.add("token", request.getToken());


        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, headers);

        ResponseEntity<IntrospectResponse> response = restTemplate.postForEntity("http://localhost:8180/auth/realms/auth-realm/protocol/openid-connect/token/introspect", httpEntity, IntrospectResponse.class);

        return new ResponseEntity<>(response.getBody(), HttpStatus.OK);
    }

    @Override
    public AccessTokenResponse loginV1(LoginRequest request) {
        AccessTokenResponse accessTokenResponse;
        Map<String, Object> credentials = new HashMap<>();
        credentials.put("secret", secret);
        AuthzClient authzClient = AuthzClient.create(new Configuration(
                authServerUrl,
                authRealm,
                clientId,
                credentials,
                null));

        try {
            log.info("Start login");
            accessTokenResponse = authzClient.obtainAccessToken(request.getUsername(), request.getPassword());
            assert accessTokenResponse != null;
            log.info("Update lasted login");
            updateUserLastLogin(accessTokenResponse);

        } catch (Exception e) {
            log.info("Error when call Keycloak CMS: ", e.getMessage());
            e.printStackTrace();
            throw new BusinessException("MessagesCode.LOGIN_FAILED");
        }
        return accessTokenResponse;
    }

    private void updateUserLastLogin(AccessTokenResponse accessToken) {

        if (StringUtils.isBlank(accessToken.getToken())) {
            return;
        }
        String userId = null;
        if (tokenProvider.parseClaims(accessToken.getToken()).get("sub") != null) {
            userId = tokenProvider.parseClaims(accessToken.getToken()).get("sub").toString();
        }
        if (null == userId) {
            return;
        }
        UserCms userCms = userInfoRepository.findByUserId(userId);
        userCms.setLastLoginDate(LocalDateTime.now());
        userInfoRepository.save(userCms);
        // update user in keycloak

//        updateUserBranchInKeycloak(userCms);
    }

    private void updateUserBranchInKeycloak(UserCms userCms) {
        UserRepresentation keycloakUser = keycloak.realm(authRealm).users().search(userCms.getUserName()).get(0);
        if (Objects.isNull(keycloakUser)) {
            throw new BusinessException("User không tồn tại trên keycloak");
        }
        // update data
        Map<String, List<String>> userAttributes = keycloakUser.getAttributes();
        userAttributes.replace("branch", List.of(userCms.getBranchId()));
        keycloakUser.setAttributes(userAttributes);
        // update keycloak
        try {
            keycloak.realm(authRealm).users().get(keycloakUser.getId()).update(keycloakUser);
        } catch (Exception e) {
            throw new BusinessException("Email không hợp lệ");
        }
    }
}
