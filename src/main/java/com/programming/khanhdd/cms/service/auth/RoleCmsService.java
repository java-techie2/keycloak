package com.programming.khanhdd.cms.service.auth;

import com.programming.khanhdd.cms.entity.role.RoleCms;
import com.programming.khanhdd.cms.model.request.permission.CreatePermissionRequest;
import com.programming.khanhdd.cms.model.request.role.CreateRoleCmsRequest;
import com.programming.khanhdd.cms.model.response.role.CreateRoleCmsResponse;
import org.keycloak.representations.idm.RoleRepresentation;


public interface RoleCmsService {
    RoleCms saveRoleCms(CreatePermissionRequest request);

    RoleRepresentation findRoleByName(String roleName);

    RoleCms findByRoleName (String roleName);

    RoleCms getRoleCmsByCode(String code);

    boolean createRole(String roleName, String description);

    boolean assignRole(String userId, String roleName);

    boolean deleteRoleUser(String userId, String roleName);

    RoleCms saveRoleCms(RoleCms roleCms);

    CreateRoleCmsResponse createRoleCms(CreateRoleCmsRequest request);
}
