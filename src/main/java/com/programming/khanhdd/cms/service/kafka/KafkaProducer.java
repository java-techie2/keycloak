package com.programming.khanhdd.cms.service.kafka;

import com.programming.khanhdd.cms.dto.CreateEmailNotificationRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaProducer {
    @Value("${spring.kafka.topics.notify}")
    private String notifyConsumeTopic;
    private final KafkaTemplate<String, CreateEmailNotificationRequest> kafkaTemplate;

    @Async
    public void produceMessageToNotify(CreateEmailNotificationRequest data) {
        try {
            kafkaTemplate.send(notifyConsumeTopic, data);
        } catch (Exception e) {
            log.error("publish message to kafka fail! Message " + data, e);
        }
    }
}
