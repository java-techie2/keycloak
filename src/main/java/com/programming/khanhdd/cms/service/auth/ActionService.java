package com.programming.khanhdd.cms.service.auth;

import com.programming.khanhdd.cms.model.response.action.ActionResponse;

import java.util.List;

public interface ActionService {
    List<ActionResponse> getAllAction();
    ActionResponse getDetailAction(String actionCode);
}
