package com.programming.khanhdd.cms.service.auth;

import com.programming.khanhdd.cms.dto.AuthorizationInformation;
import com.programming.khanhdd.cms.model.request.permission.CreatePermissionRequest;
import com.programming.khanhdd.cms.model.request.role.PushPermissionRedisRequest;
import com.programming.khanhdd.cms.model.response.permission.CreatePermissionResponse;
import com.programming.khanhdd.cms.model.response.role.PushPermissionRedisResponse;

import java.util.List;

public interface PermissionActionService {
    CreatePermissionResponse addRolePermission(CreatePermissionRequest request);

    PushPermissionRedisResponse addRoleRedis (PushPermissionRedisRequest request);

    List<AuthorizationInformation> getPermissionActionByRoleName(String roleName);
}
