package com.programming.khanhdd.cms.service.auth;

import com.programming.khanhdd.cms.entity.role.RolePermission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface RolePermissionService {
     List<RolePermission> saveAll(List<RolePermission> rolePermission);

    HashMap<String, ArrayList<String>> getPermissionOnCache(String roleCode);

    HashMap<String, ArrayList<String>> getPermissionOnCacheV1(String roleCode);
}
