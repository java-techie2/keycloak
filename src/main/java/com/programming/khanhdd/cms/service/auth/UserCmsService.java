package com.programming.khanhdd.cms.service.auth;

import com.programming.khanhdd.cms.dto.UserCmsDTO;
import com.programming.khanhdd.cms.entity.role.UserCms;
import com.programming.khanhdd.cms.model.request.user.CreateUserCmsRequest;

import java.util.List;
import java.util.Objects;

public interface UserCmsService {
    UserCms saveUser(CreateUserCmsRequest request);
    List<UserCmsDTO> findUserByUserNameOrEmailOrCifNumber(String userName, String email, String cifNumber);
    List<UserCms> findAll();
    default boolean checkUser(CreateUserCmsRequest request){
        UserCms userCms = this.saveUser(request);
        if (!Objects.isNull(userCms)){
            return true;
        }
        return false;
    }
}
