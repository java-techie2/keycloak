package com.programming.khanhdd.cms.service.noti;

import com.programming.khanhdd.cms.dto.CreateEmailNotificationRequest;

public interface NotificationService {
    void sendMailViaKafka(CreateEmailNotificationRequest data);
}
