package com.programming.khanhdd.cms;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages = "com.programming.khanhdd")
@EntityScan(basePackages = {"com.programming.khanhdd"})
@OpenAPIDefinition(
        info = @Info(
                title = "Spring boot Angular Authorization",
                version = "1.0",
                description = "Documentation Cms API v1.0",
                contact = @Contact(
                        name = "Do Duc Khanh",
                        email = "dokhanhzz12345@gmail.com"
                )
        )
)
public class CmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(CmsApplication.class, args);
    }
}
