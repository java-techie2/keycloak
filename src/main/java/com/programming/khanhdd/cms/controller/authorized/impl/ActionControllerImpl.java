package com.programming.khanhdd.cms.controller.authorized.impl;

import com.programming.khanhdd.cms.controller.authorized.ActionController;
import com.programming.khanhdd.cms.model.response.action.ActionResponse;
import com.programming.khanhdd.cms.service.auth.ActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ActionControllerImpl implements ActionController {
    @Autowired
    ActionService actionService;
    @Override
    public ResponseEntity<List<ActionResponse>> getAllAction() {
        return ResponseEntity.ok().body(actionService.getAllAction());
    }
}
