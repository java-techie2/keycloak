package com.programming.khanhdd.cms.controller.authen.impl;


import com.programming.khanhdd.cms.controller.authen.LoginController;
import com.programming.khanhdd.cms.model.request.login.LoginRequest;
import com.programming.khanhdd.cms.model.request.login.TokenRequest;
import com.programming.khanhdd.cms.model.response.login.IntrospectResponse;
import com.programming.khanhdd.cms.model.response.login.LoginResponse;
import com.programming.khanhdd.cms.model.response.login.Response;
import com.programming.khanhdd.cms.service.login.LoginService;
import com.programming.khanhdd.util.ModelMapperUtil;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginControllerImpl implements LoginController {
    @Autowired
    LoginService loginservice;

    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginrequest) {
        AccessTokenResponse accessTokenResponse = loginservice.loginV1(loginrequest);
        LoginResponse response = ModelMapperUtil.mapper(accessTokenResponse, LoginResponse.class);
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<Response> logout(@RequestBody TokenRequest token) {
        return loginservice.logout(token);
    }

    public ResponseEntity<IntrospectResponse> introspect(@RequestBody TokenRequest token) {
        return loginservice.introspect(token);
    }
}
