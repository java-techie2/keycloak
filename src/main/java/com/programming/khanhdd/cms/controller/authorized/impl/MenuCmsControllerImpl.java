package com.programming.khanhdd.cms.controller.authorized.impl;

import com.programming.khanhdd.cms.controller.authorized.MenuCmsController;
import com.programming.khanhdd.cms.model.request.menu.CreateMenuCmsRequest;
import com.programming.khanhdd.cms.model.request.menu.UpdateMenuCmsRequest;
import com.programming.khanhdd.cms.model.response.menu.CreateMenuCmsResponse;
import com.programming.khanhdd.cms.model.response.menu.UpdateMenuCmsResponse;
import com.programming.khanhdd.cms.service.auth.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuCmsControllerImpl implements MenuCmsController {
    @Autowired
    MenuService menuService;

    @Override
    public ResponseEntity<CreateMenuCmsResponse> createMenuCms(CreateMenuCmsRequest request) {
        CreateMenuCmsResponse response = menuService.createMenu(request);
        return ResponseEntity.ok().body(response);
    }

    @Override
    public ResponseEntity<UpdateMenuCmsResponse> updateMenuCms(UpdateMenuCmsRequest request) {
        return null;
    }
}
