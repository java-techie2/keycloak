package com.programming.khanhdd.cms.controller.authorized.impl;

import com.programming.khanhdd.cms.authorization.annotation.CmsPreAuthorized;
import com.programming.khanhdd.cms.constant.authorization.ActionCode;
import com.programming.khanhdd.cms.constant.authorization.MenuCode;
import com.programming.khanhdd.cms.controller.authorized.UserCmsController;
import com.programming.khanhdd.cms.entity.role.UserCms;
import com.programming.khanhdd.cms.model.request.user.CreateUserCmsRequest;
import com.programming.khanhdd.cms.service.auth.RolePermissionService;
import com.programming.khanhdd.cms.service.auth.UserCmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserCmsControllerImpl implements UserCmsController {
    @Autowired
    UserCmsService userCmsService;
    @Autowired
    RolePermissionService rolePermissionService;
    @Override
    @CmsPreAuthorized(requiredPermissions = @CmsPreAuthorized.RequiredPermission(action = ActionCode.ACTION_CREATE, menuCode = MenuCode.DASHBOARD))
    public ResponseEntity<UserCms> addNewUser(CreateUserCmsRequest request) {
        return ResponseEntity.ok(userCmsService.saveUser(request));
    }

    @Override
    @CmsPreAuthorized(requiredPermissions = @CmsPreAuthorized.RequiredPermission(action = ActionCode.ACTION_VIEW, menuCode = MenuCode.DASHBOARD))
    public ResponseEntity<List<UserCms>> getAllUsers() {
        return ResponseEntity.ok(userCmsService.findAll());
    }
}
