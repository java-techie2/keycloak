package com.programming.khanhdd.cms.controller.authorized;

import com.programming.khanhdd.cms.model.request.menu.CreateMenuCmsRequest;
import com.programming.khanhdd.cms.model.request.menu.UpdateMenuCmsRequest;
import com.programming.khanhdd.cms.model.response.menu.CreateMenuCmsResponse;
import com.programming.khanhdd.cms.model.response.menu.UpdateMenuCmsResponse;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping("/api/v1/menu")
public interface MenuCmsController {
    @PostMapping("/menuCms")
    @Operation(summary = "Thêm mới Menu")
    ResponseEntity<CreateMenuCmsResponse> createMenuCms(@Valid @RequestBody CreateMenuCmsRequest request);

    @PutMapping("/menuCms")
    @Operation(summary = "Cập nhật menu")
    ResponseEntity<UpdateMenuCmsResponse> updateMenuCms(@Valid @RequestBody UpdateMenuCmsRequest request);
}
