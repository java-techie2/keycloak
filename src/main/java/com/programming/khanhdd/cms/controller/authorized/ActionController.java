package com.programming.khanhdd.cms.controller.authorized;

import com.programming.khanhdd.cms.model.response.action.ActionResponse;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/api/v1/action")
public interface ActionController {
    @GetMapping("")
    @Operation(summary = "Lấy danh sách action")
    ResponseEntity<List<ActionResponse>> getAllAction();
}
