package com.programming.khanhdd.cms.controller.authorized;

import com.programming.khanhdd.cms.model.request.permission.CreatePermissionRequest;
import com.programming.khanhdd.cms.model.request.role.GetAllRoleCMSRequest;
import com.programming.khanhdd.cms.model.request.role.PushPermissionRedisRequest;
import com.programming.khanhdd.cms.model.response.permission.CreatePermissionResponse;
import com.programming.khanhdd.cms.model.response.role.GetAllRoleCMSResponse;
import com.programming.khanhdd.cms.model.response.role.PushPermissionRedisResponse;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/admin/authorization")
public interface AuthorizationController {
    @Operation(summary = "Tạo mới nhóm quyền truy cập")
    @PostMapping("/addPermission")
    ResponseEntity<CreatePermissionResponse> createPermission(@Valid @RequestBody CreatePermissionRequest request);

    @Operation(summary = "Lấy danh sách các quyền, phân trang")
    @GetMapping("/allRoleCms")
    ResponseEntity<List<GetAllRoleCMSResponse>> getAllRoleCmsByName(@Valid GetAllRoleCMSRequest request);

    @Operation(summary = "Đẩy permission lên redis")
    @PostMapping("/permissionToRedis")
    ResponseEntity<PushPermissionRedisResponse> permissionToRedis(@Valid @RequestBody PushPermissionRedisRequest request);
}
