package com.programming.khanhdd.cms.controller.authorized.impl;

import com.programming.khanhdd.cms.controller.authorized.AuthorizationController;
import com.programming.khanhdd.cms.model.request.permission.CreatePermissionRequest;
import com.programming.khanhdd.cms.model.request.role.GetAllRoleCMSRequest;
import com.programming.khanhdd.cms.model.request.role.PushPermissionRedisRequest;
import com.programming.khanhdd.cms.model.response.permission.CreatePermissionResponse;
import com.programming.khanhdd.cms.model.response.role.GetAllRoleCMSResponse;
import com.programming.khanhdd.cms.model.response.role.PushPermissionRedisResponse;
import com.programming.khanhdd.cms.service.auth.PermissionActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthorizationControllerImpl implements AuthorizationController {
    @Autowired
    PermissionActionService permissionActionService;

    @Override
    public ResponseEntity<CreatePermissionResponse> createPermission(CreatePermissionRequest request) {
        return ResponseEntity.ok(permissionActionService.addRolePermission(request));
    }

    @Override
    public ResponseEntity<List<GetAllRoleCMSResponse>> getAllRoleCmsByName(GetAllRoleCMSRequest request) {
        return null;
    }

    @Override
    public ResponseEntity<PushPermissionRedisResponse> permissionToRedis(PushPermissionRedisRequest request) {
        return ResponseEntity.ok(permissionActionService.addRoleRedis(request));
    }
}
