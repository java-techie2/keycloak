package com.programming.khanhdd.cms.controller.authorized;

import com.programming.khanhdd.cms.model.request.role.CreateRoleCmsRequest;
import com.programming.khanhdd.cms.model.response.role.CreateRoleCmsResponse;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/v1/role")
public interface UserRoleCmsController {
    @Operation(summary = "Thêm mới 1 role")
    @PostMapping("/createRoleCms")
    ResponseEntity<CreateRoleCmsResponse> createRoleCms(@RequestBody CreateRoleCmsRequest request);
}
