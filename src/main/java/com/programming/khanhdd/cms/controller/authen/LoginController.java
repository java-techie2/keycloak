package com.programming.khanhdd.cms.controller.authen;

import com.programming.khanhdd.cms.model.request.login.LoginRequest;
import com.programming.khanhdd.cms.model.request.login.TokenRequest;
import com.programming.khanhdd.cms.model.response.login.IntrospectResponse;
import com.programming.khanhdd.cms.model.response.login.LoginResponse;
import com.programming.khanhdd.cms.model.response.login.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/auth")
public interface LoginController {
    @PostMapping("/login")
    ResponseEntity<LoginResponse> login(LoginRequest loginrequest);

    @PostMapping("/logout")
    ResponseEntity<Response> logout(TokenRequest token);

    @PostMapping("/introspect")
    ResponseEntity<IntrospectResponse> introspect(TokenRequest token);
}
