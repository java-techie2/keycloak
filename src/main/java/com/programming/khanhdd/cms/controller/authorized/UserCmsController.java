package com.programming.khanhdd.cms.controller.authorized;

import com.programming.khanhdd.cms.entity.role.UserCms;
import com.programming.khanhdd.cms.model.request.user.CreateUserCmsRequest;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/api/v1/user")
public interface UserCmsController {
    @PostMapping("/add")
    @Operation(summary = "Thêm mới người dùng cms")
    ResponseEntity<UserCms> addNewUser(@RequestBody CreateUserCmsRequest request);

    @GetMapping("/getAll")
    @Operation(summary = "Lấy tất cả người dùng cms")
    ResponseEntity<List<UserCms>> getAllUsers();
}
