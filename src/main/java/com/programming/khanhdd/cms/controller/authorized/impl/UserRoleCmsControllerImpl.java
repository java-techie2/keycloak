package com.programming.khanhdd.cms.controller.authorized.impl;

import com.programming.khanhdd.cms.controller.authorized.UserRoleCmsController;
import com.programming.khanhdd.cms.model.request.role.CreateRoleCmsRequest;
import com.programming.khanhdd.cms.model.response.role.CreateRoleCmsResponse;
import com.programming.khanhdd.cms.service.auth.RoleCmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRoleCmsControllerImpl implements UserRoleCmsController {
    @Autowired
    RoleCmsService roleCmsService;
    @Override
    public ResponseEntity<CreateRoleCmsResponse> createRoleCms(CreateRoleCmsRequest request) {
        return ResponseEntity.ok(roleCmsService.createRoleCms(request));
    }
}
