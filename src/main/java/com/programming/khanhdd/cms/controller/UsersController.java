package com.programming.khanhdd.cms.controller;

import com.programming.khanhdd.cms.service.UserService;
import com.programming.khanhdd.cms.model.user.CreateUserRequest;
import com.programming.khanhdd.cms.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UsersController {
    @Autowired
    UserService userService;

    @GetMapping("/{id}")
    @RolesAllowed({"user", "admin"})
    public ResponseEntity<Users> getUser(@PathVariable String id) {
        return ResponseEntity.ok(userService.getUserDetail(id));
    }

    @GetMapping("")
    @RolesAllowed({"user", "admin"})
    public ResponseEntity<List<Users>> getListUser() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @PostMapping("")
    @RolesAllowed({"admin"})
    public ResponseEntity<Users> addNewUser(@RequestBody CreateUserRequest request) throws Exception {
        return ResponseEntity.ok(userService.saveUser(request));
    }


}
