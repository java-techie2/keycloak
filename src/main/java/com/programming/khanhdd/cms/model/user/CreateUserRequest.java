package com.programming.khanhdd.cms.model.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest{
    private String userName;
    private String password;
    private String email;
    private String firstName;
    private String lastName;

    public  String setFullName(){
        return this.firstName + " " + this.lastName;
    }
}
