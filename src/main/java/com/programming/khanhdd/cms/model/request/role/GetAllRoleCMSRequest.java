package com.programming.khanhdd.cms.model.request.role;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;


@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GetAllRoleCMSRequest  {
    private String roleName;
    private String roleDescription;
    private Boolean deleted;

    @NotNull
    private Boolean getAllRoleActive;


}
