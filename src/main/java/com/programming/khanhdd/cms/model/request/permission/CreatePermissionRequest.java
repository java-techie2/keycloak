package com.programming.khanhdd.cms.model.request.permission;

import com.programming.khanhdd.cms.dto.PermissionOnRole;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;


@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CreatePermissionRequest {
    @NotBlank
    private String roleName;

    @NotNull
    private Boolean roleState;

    @NotNull
    private String description;

    @NotNull
    List<PermissionOnRole> listPermissionOnRole;


}
