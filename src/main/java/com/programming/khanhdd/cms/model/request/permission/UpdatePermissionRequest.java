package com.programming.khanhdd.cms.model.request.permission;

import com.programming.khanhdd.cms.dto.PermissionOnRole;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;


@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UpdatePermissionRequest {

    private String roleId;

    private Boolean deleted; //roleState

    private String description;

    private List<PermissionOnRole> listPermission;

}
