package com.programming.khanhdd.cms.model.response.menu;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CreateMenuCmsResponse {
    private String id;
    private String menuUrl;
    private String menuCode;
    private String menuName;
    private String iconCode;
    private String parentCode;
    private String description;
}
