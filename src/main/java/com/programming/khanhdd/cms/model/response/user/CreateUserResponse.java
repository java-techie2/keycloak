package com.programming.khanhdd.cms.model.response.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserResponse {
    private String userName;
    private String id;
    private boolean enabled;
}
