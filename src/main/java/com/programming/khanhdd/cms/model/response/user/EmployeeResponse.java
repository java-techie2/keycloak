package com.programming.khanhdd.cms.model.response.user;

import com.programming.khanhdd.cms.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeResponse {
    private String id;
    private String fullName;
    private String userName;
    private String email;

    private Boolean isDelete;
    public static EmployeeResponse mapper (Employee employee){
        return EmployeeResponse.builder()
                .id(employee.getId())
                .fullName(employee.getFullName())
                .userName(employee.getUserName())
                .email(employee.getEmail())
                .isDelete(employee.getIsDelete())
                .build();
    }
}
