package com.programming.khanhdd.cms.model.request.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CreateUserCmsRequest{

    @NotBlank(message = "Mã nhân viên không được để trống")
    private String cifNumber;

    @NotBlank(message = "Tên đăng nhập không được để trống")
    private String userName;

    @NotBlank(message = "Họ và tên không được để trống")
    private String fullName;

    @NotBlank(message = "Email không được để trống")
    private String email;

    @NotBlank
    private String branchId;

    @NotBlank
    private String roleId;

    @NotBlank
    private String roleCode;

    private String description;


}
