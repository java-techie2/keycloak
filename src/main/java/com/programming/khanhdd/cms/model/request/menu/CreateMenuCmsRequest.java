package com.programming.khanhdd.cms.model.request.menu;

import com.programming.khanhdd.cms.dto.Action;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CreateMenuCmsRequest {
    @NotBlank
    private String menuUrl;
    @NotBlank
    private String menuCode;
    @NotBlank
    private String menuName;
    @NotBlank
    private String icon;
    private String parentCode;
    private String description;
    @NotBlank
    private String rank;
    private List<Action> action;
}
