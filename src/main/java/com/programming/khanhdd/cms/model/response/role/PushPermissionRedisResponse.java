package com.programming.khanhdd.cms.model.response.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PushPermissionRedisResponse {
    private String result;

}
