package com.programming.khanhdd.cms.model.response.role;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GetAllRoleCMSResponse {
    private String id;
    private String roleCode;
    private String roleName;
    private String description;
    private Boolean deleted;

}
