package com.programming.khanhdd.cms.model.request.role;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CreateRoleCmsRequest {
    private String roleCode;
    private String roleName;
    private String description;
}
