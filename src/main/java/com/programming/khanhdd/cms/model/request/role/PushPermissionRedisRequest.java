package com.programming.khanhdd.cms.model.request.role;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;


@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PushPermissionRedisRequest {
    @NotNull
    private String roleName;

}
