package com.programming.khanhdd.cms.model.request.menu;

import com.programming.khanhdd.cms.dto.ActionOnMenu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UpdateMenuCmsRequest {

    @NotBlank(message = "Đường dẫn là bắt buộc nhập")
    private String menuUrl;
    @NotBlank
    private String menuId;
    @NotBlank(message = "Menu code là bắt buộc nhập")
    private String menuCode;
    @NotBlank(message = "Tên menu là bắt buộc nhập")
    private String menuName;
    @NotBlank(message = "Icon là bắt buộc nhập")
    private String icon;
    private String parentCode;
    private String description;
    @NotBlank(message = "Số thứ tự sắp xếp là bắt buộc nhập")
    private String rank;
    @NotNull
    private Boolean deleted;
    private List<ActionOnMenu> action;

}
