package com.programming.khanhdd.cms.model.response.action;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ActionResponse {

    private String id;

    private String actionName;

    private String actionCode;

    private boolean active;
}
