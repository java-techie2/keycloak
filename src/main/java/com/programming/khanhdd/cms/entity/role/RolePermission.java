package com.programming.khanhdd.cms.entity.role;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.programming.khanhdd.audit.Persistence;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "role_permission")
@EqualsAndHashCode(callSuper = false)
public class RolePermission extends Persistence {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "role_id")
    private String roleId;

    @Column(name = "permission_id")
    private String permissionId;

    public String getId() {
        return this.id;
    }

    public void init(String createdBy) {
        this.id = NanoIdUtils.randomNanoId();
        super.init(createdBy);
    }
}
