package com.programming.khanhdd.cms.entity.role;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.programming.khanhdd.audit.Persistence;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "menu_cms")
@EqualsAndHashCode(callSuper = false)
public class MenuCms extends Persistence {
    @Id
    @Column(name = "id", columnDefinition = "VARCHAR(50)", nullable = false)
    private String id;

    @Column(name = "menu_url", columnDefinition = "VARCHAR(100)", nullable = false)
    private String menuUrl;

    @Column(name = "menu_code", columnDefinition = "VARCHAR(50)", nullable = false)
    private String menuCode;

    @Column(name = "menu_name", columnDefinition = "VARCHAR(50)", nullable = false)
    private String menuName;

    @Column(name = "description", columnDefinition = "VARCHAR(100)")
    private String description;

    @Column(name = "parent_code",columnDefinition = "VARCHAR(50)")
    private String parentCode;

    @Column(name = "icon_code",columnDefinition = "VARCHAR(550)")
    private String iconCode;

    @Column(name = "rank")
    private String rank;

    public void init(String createdBy) {
        this.id = NanoIdUtils.randomNanoId();
        super.init(createdBy);
    }


}
