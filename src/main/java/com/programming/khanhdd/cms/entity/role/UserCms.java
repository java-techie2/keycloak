package com.programming.khanhdd.cms.entity.role;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import lombok.*;
import com.programming.khanhdd.audit.Persistence;
import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_cms")
@EqualsAndHashCode(callSuper = false)
public class UserCms extends Persistence {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "cif_number")
    private String cifNumber;

    @Column(name = "username")
    private String userName;

    @Column(name = "password")
    private String password;


    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "branch_id")
    private String branchId;

    @Column(name = "is_active")
    private String isActive;


    @Column(name = "is_lock_out")
    private String isLockedOut;

    @Column(name = "dept_id")
    private String deptId;

    @Column(name = "birth_day")
    private String birthDay;

    @Column(name = "gender")
    private String gender;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "last_login_date")
    private LocalDateTime lastLoginDate;

    public void init(String createdBy) {
        this.id = NanoIdUtils.randomNanoId();
        super.init(createdBy);
    }
}