package com.programming.khanhdd.cms.entity.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "action")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ActionCms {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "action_name")
    private String actionName;

    @Column(name = "action_code")
    private String actionCode;

}