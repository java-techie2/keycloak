package com.programming.khanhdd.cms.entity.role;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.programming.khanhdd.audit.Persistence;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@Table(name = "menu_action")
@EqualsAndHashCode(callSuper = false)
public class MenuAction extends Persistence {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "menu_id")
    private String menuId;

    @Column(name = "action_id")
    private String actionId;

    @Column(name = "active")
    private Boolean active;

    public void init(String createdBy) {
        this.id = NanoIdUtils.randomNanoId();
        super.init(createdBy);
    }
}
