package com.programming.khanhdd.cms.entity.role;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.programming.khanhdd.audit.Persistence;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "role_cms")
@EqualsAndHashCode(callSuper = true)
public class RoleCms extends Persistence {
    @Id
    @Column(name = "id", columnDefinition = "VARCHAR(50)",
            updatable = false,
            nullable = false)
    private String id;

    @Column(name = "role_code", columnDefinition = "VARCHAR(50)", nullable = false)
    private String roleCode;

    @Column(name = "role_name", columnDefinition = "VARCHAR(50)", nullable = false)
    private String roleName;

    @Column(name = "description", columnDefinition = "VARCHAR(500)", nullable = false)
    private String description;

    public void init(String createdBy) {
        this.id = NanoIdUtils.randomNanoId();
        super.init(createdBy);
    }
}