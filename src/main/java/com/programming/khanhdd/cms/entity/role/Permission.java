package com.programming.khanhdd.cms.entity.role;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.programming.khanhdd.audit.Persistence;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "permission")
@EqualsAndHashCode(callSuper = false)
public class Permission extends Persistence {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "action")
    private String action;

    @Column(name = "action_name")
    private String actionName;

    @Column(name = "menu_id")
    private String menuId;

    @Column(name = "deleted")
    private boolean deleted;

    public void init(String createdBy) {
        this.id = NanoIdUtils.randomNanoId();
        super.init(createdBy);
    }

}

