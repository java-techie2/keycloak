package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.role.MenuCms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuRepository extends JpaRepository<MenuCms, String> {
    @Query(value = "Select * from menu_cms where id = :id", nativeQuery = true)
    MenuCms getById(String id);
}
