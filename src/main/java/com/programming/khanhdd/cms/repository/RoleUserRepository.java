package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.role.RoleUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleUserRepository extends JpaRepository<RoleUser, String> {


}
