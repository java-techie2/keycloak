package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.role.RoleCms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleCmsRepository extends JpaRepository<RoleCms, String> {

    RoleCms findRoleCmsByRoleName(String roleName);

    @Query(value = "select * from role_cms urc where urc.deleted = false and urc.role_code = :code", nativeQuery = true)
    RoleCms getCmsRoleByCode(String code);
}
