package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.role.MenuAction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuActionRepository extends JpaRepository<MenuAction, String> {
}
