package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.dto.UserCmsDTO;
import com.programming.khanhdd.cms.entity.role.UserCms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCmsRepository extends JpaRepository<UserCms, String> {
    UserCms findByUserId(String id);
    @Query(value = "select new com.programming.khanhdd.cms.dto.UserCmsDTO(uc.userName,uc.email,uc.cifNumber) " +
            "from UserCms as uc " +
            "where uc.userName = :userName " +
            "or uc.email = :email " +
            "or uc.cifNumber = :cifNumber")
    List<UserCmsDTO> findByUserNameOrEmailOrCifNumber(String userName, String email, String cifNumber);
}
