package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.role.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, String> {

    List<RolePermission> getRolePermissionByRoleId(String roleId);

}
