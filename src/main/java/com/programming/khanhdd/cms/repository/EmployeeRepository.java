package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {

    List<Employee> getAllByIsDelete(Boolean isDelete);

    Employee getEmployeeByIdAndIsDelete(String id, Boolean isDelete);

    boolean existsByEmailAndIsDelete(String email, Boolean isDelete);

    boolean existsByUserNameAndIsDelete(String userName, Boolean isDelete);
}
