package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.role.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.programming.khanhdd.cms.dto.AuthorizationInformation;
import java.util.ArrayList;
import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, String> {
    @Query(value = "Select * from  permission where menu_id = :menuId and action = :action", nativeQuery = true)
    Permission findByMenuIdAndAction(String menuId, String action);

    @Query(value = "Select * from  permission where id in (:ids)", nativeQuery = true)
    List<Permission> findPermission(List<String> ids);

    ArrayList<Permission> findPermissionsById(String id);

    Permission findPermissionById(String id);

    @Query(value = "SELECT new \n" +
            "   com.programming.khanhdd.cms.dto.AuthorizationInformation(mc.menuCode, p.action)\n" +
            "FROM\n" +
            "    RoleCms rc,\n" +
            "    RolePermission rp,\n" +
            "    Permission p,\n" +
            "    MenuCms mc\n" +
            "WHERE\n" +
            "    rp.roleId = rc.id\n" +
            "        AND rp.permissionId = p.id\n" +
            "        AND p.menuId = mc.id\n" +
            "        AND rp.deleted = FALSE\n" + // quy ước với bên FE trạng thái bảng rolePermission = True là hoạt động, có quyền
            "        AND mc.deleted = FALSE\n" +
            "        and rc.roleName = :roleName")
    List<AuthorizationInformation> getPermissionActionByRoleName(String roleName);

}
