package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.dto.UserRoleViewEntity;
import com.programming.khanhdd.cms.entity.role.RoleUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRoleViewEntity, String> {
    @Query(nativeQuery = true)
    List<UserRoleViewEntity> getAllByUserName(@Param("userName") String userName);

}
