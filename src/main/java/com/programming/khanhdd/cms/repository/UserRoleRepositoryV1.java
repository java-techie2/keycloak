package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.dto.UserRoleProcedureEntity;
import com.programming.khanhdd.cms.dto.UserRoleViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepositoryV1 extends JpaRepository<UserRoleProcedureEntity, String> {
    @Procedure("find_user")
    List<UserRoleProcedureEntity> getAllByUserNameProcedure(@Param("userName") String userName);

}
