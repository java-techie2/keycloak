package com.programming.khanhdd.cms.repository;

import com.programming.khanhdd.cms.entity.role.ActionCms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionRepository extends JpaRepository<ActionCms, String> {
    ActionCms getActionCmsByActionCode(String actionCode);
}
