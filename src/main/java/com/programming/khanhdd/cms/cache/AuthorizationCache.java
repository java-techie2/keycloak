package com.programming.khanhdd.cms.cache;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.programming.khanhdd.cache.RedisService;
import com.programming.khanhdd.cms.dto.AuthorizationInformation;
import com.programming.khanhdd.util.JsonService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class AuthorizationCache {
    private static final String ROLE = "ROLE_";
    private final RedisService redisService;

    public AuthorizationCache(RedisService redisService) {
        this.redisService = redisService;
    }

    public HashMap<String, ArrayList<String>> getInfoRolePermissionOnCache(String roleCode) {
        Object obj = redisService.get(ROLE + roleCode);
        if (null == obj) {
            return null;
        }
        return JsonService.parseJsonToObject((String) obj, HashMap.class);
    }

    public void pushInfoRolePermissionOnCache(String roleCode, String data) {
        redisService.set(ROLE + roleCode, data);
    }

    public String buildData(List<AuthorizationInformation> authorizationInformations) throws JsonProcessingException {
        if (null == authorizationInformations || authorizationInformations.isEmpty()) {
            return null;
        }
        Map<String, List<String>> data = new HashMap<>();
        for (AuthorizationInformation element : authorizationInformations) {
            if (!data.containsKey(element.getMenuCode())) {
                List<String> listAction = new ArrayList<>();
                listAction.add(element.getAction());
                data.put(element.getMenuCode(), listAction);
            } else {
                List<String> listAction = data.get(element.getMenuCode());
                listAction.add(element.getAction());
                data.put(element.getMenuCode(), listAction);
            }

        }

        return new ObjectMapper().writeValueAsString(data);
    }
}
