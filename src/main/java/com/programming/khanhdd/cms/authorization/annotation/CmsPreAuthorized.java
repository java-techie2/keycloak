package com.programming.khanhdd.cms.authorization.annotation;

import com.programming.khanhdd.cms.constant.authorization.ActionCode;
import com.programming.khanhdd.cms.constant.authorization.MenuCode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CmsPreAuthorized {
    RequiredPermission[] requiredPermissions() default {};

    @interface RequiredPermission {
        MenuCode menuCode();
        ActionCode[] action();
    }
}
