package com.programming.khanhdd.cms.authorization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.programming.khanhdd.cms.authorization.annotation.CmsPreAuthorized;
import com.programming.khanhdd.cms.constant.authorization.ActionCode;
import com.programming.khanhdd.cms.service.auth.RolePermissionService;
import com.programming.khanhdd.exception.BusinessException;
import com.programming.khanhdd.security.jwt.JwtTokenProvider;
import com.programming.khanhdd.security.jwt.JwtUtil;
import com.programming.khanhdd.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

@Aspect
@Slf4j
@AllArgsConstructor
@Component
public class CmsAuthorizationAdvice {

    private final JwtTokenProvider tokenProvider;
    private final RolePermissionService rolePermissionCommandService;

    @Before("within(com.programming.khanhdd.cms..*) && @annotation(com.programming.khanhdd.cms.authorization.annotation.CmsPreAuthorized)")
    public void handlePreAuthorize(JoinPoint joinPoint) throws NoSuchMethodException, JsonProcessingException {

        CmsPreAuthorized cmsPreAuthorized = AspectUtil.getAnnotationFromJoinPoint(joinPoint, CmsPreAuthorized.class);
        CmsPreAuthorized.RequiredPermission[] requiredPermissions = cmsPreAuthorized.requiredPermissions();

        // lấy role từ token
        String token;
        try {
            token = JwtUtil.getBearerToken();
        } catch (Exception e) {
            log.error("PreAuthorize error", e);
            throw new BusinessException("User không có quyền truy cập api này");
        }

        checkAllRoleHasPermission(requiredPermissions, token);
    }

    private void checkAllRoleHasPermission(CmsPreAuthorized.RequiredPermission[] requiredPermissions, String token) {

        Collection<GrantedAuthority> role = tokenProvider.getRolesFromToken(token);
        if (role.size() == 0) {
            return;
        }
        HashMap<String, ArrayList<String>> permission;
        boolean hasPermission = false;
        for (GrantedAuthority element : role) {
//            permission = rolePermissionCommandService.getPermissionOnCache(element.toString().trim().replace(StringUtil.SPACE, StringUtil.UNDERSCORE));
            permission = rolePermissionCommandService.getPermissionOnCacheV1(element.toString().trim().replace(StringUtil.SPACE, StringUtil.UNDERSCORE));
            if (null != permission) {
                hasPermission = checkPermission(requiredPermissions, permission);
            }
            if (hasPermission) {
                break;
            }
        }
        if (!hasPermission) {
            throw new BusinessException("User không có quyền truy cập api này");
        }
    }


    private boolean checkPermission(CmsPreAuthorized.RequiredPermission[] requiredPermissions,
                                    HashMap<String, ArrayList<String>> permission) {

        for (CmsPreAuthorized.RequiredPermission element : requiredPermissions) {
            //check has permission active in menu
            ArrayList<String> listAction = permission.get(element.menuCode().getValue());
            if (null != listAction && !listAction.isEmpty()) {
                String[] getAction = toArrayString(element.action());
                for (String action : getAction) {
                    if (listAction.contains(action)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static String[] toArrayString(ActionCode[] array) {
        return Arrays.stream(array).map(ActionCode::getActionCode).toArray(String[]::new);
    }
}
