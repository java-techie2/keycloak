package com.programming.khanhdd.cms.authorization;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public final class AspectUtil {

    public static <T extends Annotation> T getAnnotationFromJoinPoint(JoinPoint joinPoint, Class<T> clazz)
            throws NoSuchMethodException {
        Method interfaceMethod = ((MethodSignature) joinPoint.getSignature()).getMethod();
        Method implementMethod = joinPoint.getTarget()
                .getClass()
                .getMethod(
                        interfaceMethod.getName(),
                        interfaceMethod.getParameterTypes());
        if (implementMethod.isAnnotationPresent(clazz)) {
            return implementMethod.getAnnotation(clazz);
        }
        throw new IllegalArgumentException("JoinPoint was not annotated by annotation: " + clazz.toString());
    }

}
