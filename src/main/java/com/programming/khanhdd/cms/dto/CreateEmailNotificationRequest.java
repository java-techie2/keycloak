package com.programming.khanhdd.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateEmailNotificationRequest {
    private String email;
    private String password;
    private String userName;
}
