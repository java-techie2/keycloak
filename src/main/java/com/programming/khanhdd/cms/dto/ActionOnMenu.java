package com.programming.khanhdd.cms.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ActionOnMenu {
    private String id; // menu_id
    private String actionCode;
    private boolean active;
    private String actionName;

    public ActionOnMenu(String id, String actionCode, boolean active, String actionName) {
        this.id = id;
        this.actionCode = actionCode;
        this.active = active;
        this.actionName = actionName;
    }

    public ActionOnMenu(String id, String actionCode, boolean active) {
        this.id = id;
        this.actionCode = actionCode;
        this.active = active;
    }
}
