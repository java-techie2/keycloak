package com.programming.khanhdd.cms.dto;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PermissionAction {
    private String id; // id trong bản ghi role_permission
    private String action;
    private boolean deleted; // trạng thái của bản ghi trong bảng permission ; hiện do logic đang để deleted = true là đã được chọn, deleted = false là chưa được chọn
    private boolean active; // actionCms này có hoạt động trên 1 menu hay không

    public static String init() {
        return NanoIdUtils.randomNanoId();
    }

}
