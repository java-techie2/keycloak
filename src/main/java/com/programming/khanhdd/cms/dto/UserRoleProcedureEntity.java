package com.programming.khanhdd.cms.dto;

import lombok.Data;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;

@Entity
@Data
@NamedStoredProcedureQueries(
        @NamedStoredProcedureQuery(name = "userRoleProcedure", procedureName = "tmp_find_user",
                parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "userName", type = String.class) }))
public class UserRoleProcedureEntity {
    @Id
    @Column(name = "id")
    private String id;
    private String userName;
    private String roleName;
    private String roleCode;

}
