package com.programming.khanhdd.cms.dto;

import lombok.Data;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;

@Entity
@SqlResultSetMapping(
        name = "userRoleMapping",
        entities = {
                @EntityResult(entityClass = UserRoleViewEntity.class)
        }
)
@Data
@NamedNativeQuery(
        name = "UserRoleViewEntity.getUserRole",
        query = "SELECT * FROM vw_user_role",
        resultSetMapping = "userRoleMapping"
)
@Subselect("select a.* from vw_user_role a ")
public class UserRoleViewEntity {
    @Id
    @Column(name = "id")
    private String id;
    private String userName;
    private String roleName;
    private String roleCode;

}
