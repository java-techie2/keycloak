package com.programming.khanhdd.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCmsDTO {

    @Column(name = "username")
    private String userName;

    @Column(name = "email")
    private String email;

    @Column(name = "cif_number")
    private String cifNumber;
}
