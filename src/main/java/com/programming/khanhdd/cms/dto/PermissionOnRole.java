package com.programming.khanhdd.cms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PermissionOnRole {
    List<PermissionAction> listAction;
    String idMenu;
    String menuUrl;
    String menuCode;
    String menuName;
    String parentCode;

}

