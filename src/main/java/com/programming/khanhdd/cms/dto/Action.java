package com.programming.khanhdd.cms.dto;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Action {
    private String id;
    private String actionName;
    private String actionCode;
    private Boolean active;

    public static String init() {
        return NanoIdUtils.randomNanoId();
    }

}
