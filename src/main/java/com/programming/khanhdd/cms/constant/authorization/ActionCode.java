package com.programming.khanhdd.cms.constant.authorization;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum ActionCode {

    ACTION_VIEW("view"),
    ACTION_UPDATE("update"),
    ACTION_APPROVE("approve"),
    ACTION_CREATE("create"),
    ACTION_DELETE("delete"),
    ;

    private final String actionCode;

    private ActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String toString() {
        return this.actionCode;
    }

    public static String[] getAction(Class<? extends Enum<?>> e) {
        return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }
}
