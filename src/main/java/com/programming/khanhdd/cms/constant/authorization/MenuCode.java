package com.programming.khanhdd.cms.constant.authorization;

public enum MenuCode {
    /**
     * Dashboard
     */
    DASHBOARD("dashboard"), //Dashboard/
    MENU_MANAGEMENT("quan_ly_menu"), //Quản lý Menu
    MANAGE_ACCSESS("quan_ly_quyen_truy_cap"), //Quản lý quyền truy cập

    /**
     * Khách hàng
     */
    CLIENT("khach_hang"), //Khách hàng
    OPEN_ACCOUNT_VNPOST("kh_mo_tk_qua_vnpost"), //KH mở TK qua VNPOST
    PASSWORD_RECOVERY("khoi_phuc_mat_khau"), //Khôi phục mật khẩu
    RECONCILIATION("doi_soat_payoo"), //Đối soát/Payoo
    APPROVAL_WAITING_LIST("danh_sach_cho_duyet"), //Danh sách chờ duyệt
    REQUEST_LIST("danh_sach_yeu_cau"), //Danh sách yêu cầu
    CUSTOMER_INFORMATION("thong_tin_khach_hang"), //Thông tin khách hàng
    APPROVE_CUSTOMER_INFORMATION_STM("duyet_thong_tin_kh_tu_stm"), //Duyệt thông tin KH từ STM
    MORTGAGE_LOAN_ONLINE_SAVING("quan_ly_hs_vay_tktt"), // Quản lý hồ sơ vay cầm cố TGTK trực tuyến
    // quản lý khách hàng gói dịch vụ khách hàng
    CUSTOMER_CASH_LIMIT_PACKAGE("quan_ly_han_muc_kh"),

    /**
     * Quản lý nội dung NH số
     */
    DIGITAL_BANKING_CONTENT_MANAGEMENT("quan_ly_nd_kh_so"), //Quản lý nội dung NH số
    ERROR_CODE_MANAGEMENT("quan_ly_ma_loi"), //Quản lý mã lỗi
    MANAGE_THEMES("quan_ly_themes"), //Quản lý appearance
    BANK_CODE_LIST("quan_ly_ma_ngan_hang"), //Danh mục mã ngân hàng
    MAP_MANAGEMENT("quan_ly_map"), //Quản lý bảng MAP
    VACATION_DAY("quan_ly_ngay_lam_viec"), //Quản lý bảng MAP
    NOTIFI_TEMPLATE_MANAGEMENT("quan-ly-template"), //Quản lý bảng MAP
    REFERRAL_MANAGEMENT("quan_ly_ma_gioi_thieu"), // Quản lý mã giới thiệu
    PRODUCT_MANAGEMENT("quan_ly_san_pham"), //Quản lý sản phẩm
    USER_PRODUCT_REFERRAL_MANAGEMENT("phe_duyet_gan_san_pham"), //Phê duyệt gán sản phẩm
    THEME_MANAGEMENT("quan_ly_themes"),
    /**
     * Quản lý hoa hồng
     */
    APPROVE_COMMISSION("phe_duyet_chi_hoa_hong"),
    COMMISSION_LIST("ds_chi_hoa_hong"),
    /**
     * Hoa hồng
     */
    COMMISSION_ACCOUNTING_APPROVE("phe_duyet_chi_hoa_hong"),
    COMMISSION_ACCOUNTING_PAYMENT("ds_chi_hoa_hong"),

    /**
     * Giao dịch đáng ngờ
     */
    SUSPICIOUS_TRANSACTION("giao_dich_dang_ngo"), //Giao dịch đáng ngờ
    EKYC("ekyc"), //eKYC
    EKYC_APPROVE("xac_nhan_ekyc"), //xac_nhan_ekyc
    EKYC_TRACKING("theo_doi_ekyc"), //xac_nhan_ekyc
    CASH_LIMIT_FOR_SUSPICIOUS_EKYC("han_muc_cho_suspicious_ekyc"),
    CASH_LIMIT_FOR_USER_EKYC("han_muc_cho_kh_ekyc"),

    /**
     * Video call
     */
    APPROVE_VIDEO_CALL("approve_video_call"),
    /**
     * Báo cáo
     */
    TRADING_REPORT("bao_cao_kinh_doanh"), // Báo cáo kinh doanh
    EBANK_REPORT("bao_cao_khach_hang"), // Báo cáo khách hàng
    REPORT_TKSD("bao_cao_tksd"), // Bao cao TKSD
    ;

    public String getValue() {
        return menuCode;
    }

    private final String menuCode;

    MenuCode(String menuCode) {
        this.menuCode = menuCode;
    }
}
