package com.programming.khanhdd.cms.constant.authorization;

import lombok.Getter;

@Getter
public enum TypeCheckValidateMenu {

    MENU_URL,
    MENU_CODE,
    MENU_RANK,
    MENU_NAME
    ;

}
