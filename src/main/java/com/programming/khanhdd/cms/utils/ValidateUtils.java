package com.programming.khanhdd.cms.utils;

import com.programming.khanhdd.exception.BusinessException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtils {
    public static final String PHONE_REGEX = "((09|03|07|08|05)+([0-9]{8})\\b)";
    public static boolean validateEmail(String email) {
        String regexEmail = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regexEmail);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void validatePassword(String password) {
        if (password.length() < 6) {
            throw new BusinessException("Độ dài password không hợp lệ");
        }
        Pattern letter = Pattern.compile("[a-zA-z]");
        Pattern digit = Pattern.compile("[0-9]");
        Pattern special = Pattern.compile("[!@#$%^*]");
        Matcher hasLetter = letter.matcher(password);
        Matcher hasDigit = digit.matcher(password);
        Matcher hasSpecial = special.matcher(password);
        boolean hasUppercase = password.equals(password.toLowerCase());
        boolean hasLowercase = password.equals(password.toUpperCase());
        if (!hasLetter.find() || !hasDigit.find() || !hasSpecial.find() || hasUppercase == true || hasLowercase == true) {
            throw new BusinessException("Password không đúng định dạng");
        }
    }

    public static boolean validatePhone(String phone) {
        String regexPhone = "((09|03|07|08|05)+([0-9]{8})\\b)";
        Pattern pattern = Pattern.compile(regexPhone);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }
}
